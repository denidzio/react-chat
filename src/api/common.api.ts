import IRequest from "../interfaces/IRequest";

const request = async <T>(request: IRequest): Promise<T | null> => {
  const { url, method, body } = request;

  const headers = {
    Accept: "application/json, text/plain, */*",
  };

  const response = await fetch(url, {
    method,
    headers,
    body: JSON.stringify(body),
  });

  if (response.ok) {
    return await response.json();
  }

  throw new Error("Fetch error");
};

export const get = async <T>(url: string): Promise<T | null> => {
  return await request({ url, method: "GET" });
};
