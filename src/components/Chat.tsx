import React, { Component } from "react";
import IUser from "../interfaces/IUser";
import { get } from "../api/common.api";
import IMessage from "../interfaces/IMessage";
import { Header, MessageInput, MessageList, Preloader } from "./";
import data from "../fakeData/messages.json";
import {
  addMessage,
  createMessage,
  deleteMessage,
  editMessage,
  likeMessage,
  replaceMessage,
} from "../helpers/message.helper";

const me: IUser = {
  id: "533b5230-1b8f-11e8-9629-c7eca82aa7bd",
  name: "Denys",
  avatar: "https://i.ibb.co/vhmLVJQ/ava.jpg",
};

type ChatProps = {
  url: string;
};

type ChatState = {
  isLoad: boolean;
  messages: IMessage[];
  user: IUser | null;
  editMessage: IMessage | null;
};

export default class Chat extends Component<ChatProps, ChatState> {
  constructor(props: ChatProps) {
    super(props);

    this.state = {
      isLoad: true,
      messages: [],
      user: null,
      editMessage: null,
    };

    this.handleDeleteMessage = this.handleDeleteMessage.bind(this);
    this.handleInputMessage = this.handleInputMessage.bind(this);
    this.handleEditMessage = this.handleEditMessage.bind(this);
    this.handleStartEdit = this.handleStartEdit.bind(this);
    this.handleLike = this.handleLike.bind(this);
  }

  async componentDidMount() {
    const messages = await get<IMessage[]>(this.props.url);

    if (messages) {
      this.setState({ messages });
    }

    // this.setState({ messages: data });
    this.setState({ user: me, isLoad: false });
  }

  handleInputMessage(message: string) {
    if (!this.state.user) {
      return;
    }

    const newMessage = createMessage({ user: this.state.user, text: message });

    this.setState((prevState) => ({
      messages: addMessage(prevState.messages, newMessage),
    }));
  }

  handleStartEdit(message: IMessage) {
    this.setState((prevState) => {
      if (!prevState.editMessage) {
        return { editMessage: message };
      }

      if (prevState.editMessage.id === message.id) {
        return { editMessage: null };
      }

      return { editMessage: message };
    });
  }

  handleEditMessage(message: string) {
    if (!this.state.editMessage) {
      return;
    }

    const editedMessage = editMessage(this.state.editMessage, message);

    this.setState((prevState) => ({
      messages: replaceMessage(prevState.messages, editedMessage),
      editMessage: null,
    }));
  }

  handleDeleteMessage(messageId: string) {
    this.setState((prevState) => ({
      messages: deleteMessage(prevState.messages, messageId),
    }));
  }

  handleLike(messageId: string) {
    this.setState((prevState) => ({
      messages: likeMessage(prevState.messages, messageId),
    }));
  }

  render() {
    if (this.state.isLoad) {
      return <Preloader />;
    }

    if (!this.state.messages || !this.state.user) {
      return null;
    }

    return (
      <div className="chat">
        <Header messages={this.state.messages} />
        <MessageList
          messages={this.state.messages}
          userId={this.state.user.id}
          onDelete={this.handleDeleteMessage}
          onEdit={this.handleStartEdit}
          onLike={this.handleLike}
        />
        <MessageInput
          onSend={this.handleInputMessage}
          onEdit={this.handleEditMessage}
          editMessage={this.state.editMessage?.text}
        />
      </div>
    );
  }
}
