import React, { ChangeEvent, Component, SyntheticEvent } from "react";
import { createRef } from "react";

type MessageInputProps = {
  editMessage: string | undefined;
  onSend: (message: string) => void;
  onEdit: (message: string) => void;
};

type MessageInputState = { message: string };

export default class MessageInput extends Component<
  MessageInputProps,
  MessageInputState
> {
  inputRef = createRef<HTMLInputElement>();

  constructor(props: MessageInputProps) {
    super(props);

    this.state = { message: "" };

    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps: MessageInputProps) {
    const editMessage = this.props.editMessage;

    if (editMessage === undefined && prevProps.editMessage) {
      this.setState({ message: "" });
    }

    if (editMessage !== undefined && prevProps.editMessage !== editMessage) {
      this.setState({ message: editMessage });
      this.inputRef.current?.focus();
    }
  }

  handleInput(e: ChangeEvent<HTMLInputElement>) {
    this.setState({ message: e.currentTarget.value });
  }

  handleSubmit(e: SyntheticEvent) {
    e.preventDefault();

    if (!this.state.message.trim()) {
      return;
    }

    if (this.props.editMessage) {
      this.props.onEdit(this.state.message);
    } else {
      this.props.onSend(this.state.message);
    }

    this.setState({ message: "" });
  }

  render() {
    return (
      <form className="message-input" onSubmit={this.handleSubmit}>
        <label htmlFor="message" className="message-input-label">
          <input
            ref={this.inputRef}
            onChange={this.handleInput}
            value={this.state.message}
            type="text"
            className="message-input-text"
            id="message"
            placeholder="Enter your message..."
            autoComplete="off"
          />
        </label>
        <button
          type="submit"
          className="message-input-button message-input__button"
        >
          {this.props.editMessage ? "Edit" : "Send"}
        </button>
      </form>
    );
  }
}
