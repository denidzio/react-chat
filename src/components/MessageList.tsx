import React, { Component, Fragment, createRef } from "react";
import { getDiffDays, getDividerText } from "../helpers/chat.helper";

import IMessage from "../interfaces/IMessage";
import { Message, OwnMessage } from "./";

type MessageListProps = {
  messages: IMessage[];
  userId: string;
  onLike: (messageId: string) => void;
  onDelete: (messageId: string) => void;
  onEdit: (message: IMessage) => void;
};
type MessageListState = {};

export default class MessageList extends Component<
  MessageListProps,
  MessageListState
> {
  listRef = createRef<HTMLDivElement>();

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate(prevProps: MessageListProps) {
    if (this.props.messages.length > prevProps.messages.length) {
      this.scrollToBottom();
    }
  }

  scrollToBottom() {
    const node = this.listRef.current;

    if (!node) {
      return;
    }

    node.scrollTop = node.scrollHeight;
  }

  renderDivider(currentMsg: IMessage, prevMsg: IMessage) {
    if (prevMsg && getDiffDays(currentMsg.createdAt, prevMsg.createdAt) === 0) {
      return null;
    }

    return (
      <div className="messages-divider">
        {getDividerText(currentMsg.createdAt)}
      </div>
    );
  }

  render() {
    const messages = this.props.messages;

    return (
      <div className="message-list" ref={this.listRef}>
        {messages.map((message, index) => (
          <Fragment key={message.id}>
            {this.renderDivider(message, messages[index - 1])}
            {message.userId === this.props.userId ? (
              <OwnMessage
                message={message}
                onDelete={this.props.onDelete}
                onEdit={this.props.onEdit}
              />
            ) : (
              <Message message={message} onLike={this.props.onLike} />
            )}
          </Fragment>
        ))}
      </div>
    );
  }
}
