import IMessage from "../interfaces/IMessage";
import moment from "moment";
import "moment/locale/en-gb";

export const getNumberOfChatMembers = (messages: IMessage[]) => {
  const uniqUsersId = new Set();

  for (const message of messages) {
    uniqUsersId.add(message.userId);
  }

  return uniqUsersId.size;
};

export const truncateDate = (date: string) => {
  return moment.utc(date).startOf("day").format();
};

export const getHeaderDate = (date: string) => {
  // const diffDays = getDiffDays(moment.utc().format(), date);

  // if (diffDays === 0) {
  //   return moment.utc(date).format("HH:mm");
  // }

  return moment.utc(date).format("DD.MM.YYYY HH:mm");
};

export const getMessageTime = (date: string) => {
  return moment.utc(date).format("HH:mm");
};

export const getDiffDays = (date_1: string, date_2: string) => {
  const moment_1 = moment(truncateDate(date_1));
  const moment_2 = moment(truncateDate(date_2));

  return Math.abs(moment_1.diff(moment_2, "days"));
};

export const getDividerText = (messageDate: string) => {
  const diffDays = getDiffDays(moment.utc().format(), messageDate);

  if (diffDays === 0) {
    return "Today";
  }

  if (diffDays === 1) {
    return "Yesterday";
  }

  moment.locale("en-gb");

  return moment
    .utc(messageDate)
    .format("LLLL")
    .split(" ")
    .slice(0, 3)
    .join(" ");
};
