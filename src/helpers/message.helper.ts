import moment from "moment";
import IMessage from "../interfaces/IMessage";
import IUser from "../interfaces/IUser";
import { generateId } from "./id.helper";

export const createMessage = ({
  user,
  text,
}: {
  user: IUser;
  text: string;
}): IMessage => {
  return {
    text,
    id: generateId(),
    user: user.name,
    userId: user.id,
    avatar: user.avatar,
    createdAt: moment().format(),
    editedAt: "",
  };
};

export const addMessage = (messages: IMessage[], message: IMessage) => {
  return [...messages, message];
};

export const deleteMessage = (messages: IMessage[], messageId: string) => {
  return messages.filter((message) => message.id !== messageId);
};

export const editMessage = (message: IMessage, text: string) => {
  return { ...message, text, editedAt: new Date().toISOString() };
};

export const likeMessage = (messages: IMessage[], messageId: string) => {
  const message = messages.find((message) => message.id === messageId);

  if (!message) {
    return messages;
  }

  return replaceMessage(messages, { ...message, liked: !message.liked });
};

export const replaceMessage = (messages: IMessage[], message: IMessage) => {
  const replacedMessageId = messages.findIndex((m) => m.id === message.id);

  if (replacedMessageId !== -1) {
    return messages
      .slice(0, replacedMessageId)
      .concat(message, messages.slice(replacedMessageId + 1));
  }

  return messages;
};
