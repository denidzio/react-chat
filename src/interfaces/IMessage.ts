interface IMessage {
  id: string;
  user: string;
  userId: string;
  text: string;
  avatar: string;
  liked?: boolean;
  createdAt: string;
  editedAt: string;
}

export default IMessage;
