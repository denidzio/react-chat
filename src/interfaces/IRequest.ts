interface IRequest {
  url: string;
  method: string;
  body?: any;
}

export default IRequest;
